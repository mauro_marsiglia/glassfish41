/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var map;
var untiled;
var tiled;
var pureCoverage = false;
// pink tile avoidance
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
// make OL compute scale according to WMS spec
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
var layer;
var lon = -74.78132;
var lat = 10.96854;
var zoom = 15;

$(document).ready(function() {

    initMap();


});

function initMap() {
    var optionsArcgGis = {
        controls: [],
        projection: "EPSG:3857",
        displayProjection: "EPSG:4326",
        units: 'degrees',
        numZoomLevels: 20
    };
    map = new OpenLayers.Map('map', optionsArcgGis);
    var ArcGislayer = new OpenLayers.Layer.WMTS(
            {
                name: 'ARC GIS',
                url: 'http://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/WMTS',
                layer: 'Citations',
                style: "default",
                format: 'image/png',
                matrixSet: "matrix_id"
            },
    {
        format: 'image/png',
        isBaseLayer: true
    });
    //map.addLayer(ArcGislayer);

    var alcaldiaBaqLayersPasosCaminos = new OpenLayers.Layer.WMS(
            "Pasos caminos", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '5',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );

    var alcaldiaBaqLayersInstitucionSalud = new OpenLayers.Layer.WMS(
            "Instituciones de salud", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '6',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );

    var alcaldiaBaqLayersEstacionesPolicia = new OpenLayers.Layer.WMS(
            "Estaciones de policía", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '7',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );


    var alcaldiaBaqLayersHogaresInfantiles = new OpenLayers.Layer.WMS(
            "Hogares infantiles", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '8',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );


    var alcaldiaBaqLayersEducacionSuperior = new OpenLayers.Layer.WMS(
            "Educación superior", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '9',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );


    var alcaldiaBaqLayersComisariasSeg = new OpenLayers.Layer.WMS(
            "Comisarías de seguridad", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '10',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );


    var alcaldiaBaqLayersColegiosPublicos = new OpenLayers.Layer.WMS(
            "Colegios públicos", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '11',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );


    var alcaldiaBaqLayersCentrosVida = new OpenLayers.Layer.WMS(
            "Centros de vida", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '12',
                "STYLES": '',
                transparent: "true",
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );

    var stationsLayer = new OpenLayers.Layer.WMS(
            "ESTACIONES", "http://webavl.bomberos.tecnosystems.co:80/geoserver/firefighters_dev_ws/wms",
            {
                "LAYERS": 'firefighters_dev_ws:estaciones',
                transparent: "true",
                "STYLES": '',
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        singleTile: true,
        yx: {'EPSG:4326': true}
    }
    );

    // setup tiled layer
    var machinesLayer = new OpenLayers.Layer.WMS(
            "MAQUINAS", "http://webavl.bomberos.tecnosystems.co:80/geoserver/firefighters_dev_ws/wms",
            {
                "LAYERS": 'firefighters_dev_ws:maquinas',
                transparent: "true",
                "STYLES": '',
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        singleTile: true,
        yx: {'EPSG:4326': true}
    }
    );

    // setup tiled layer
    var casesLayer = new OpenLayers.Layer.WMS(
            "CASOS", "http://webavl.bomberos.tecnosystems.co:80/geoserver/firefighters_dev_ws/wms",
            {
                "LAYERS": 'firefighters_dev_ws:casos',
                transparent: "true",
                "STYLES": '',
                format: 'image/png'
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        singleTile: true,
        yx: {'EPSG:4326': true}
    }
    );

    //map.addLayer(alcaldiaBaqLayersTheme);

    map.addLayers([ArcGislayer,
        alcaldiaBaqLayersPasosCaminos,
        alcaldiaBaqLayersInstitucionSalud,
        alcaldiaBaqLayersEstacionesPolicia,
        alcaldiaBaqLayersHogaresInfantiles,
        alcaldiaBaqLayersEducacionSuperior,
        alcaldiaBaqLayersComisariasSeg,
        alcaldiaBaqLayersColegiosPublicos,
        alcaldiaBaqLayersCentrosVida,
        stationsLayer,
        machinesLayer,
        casesLayer]);

    // build up all controls
    map.addControl(new OpenLayers.Control.PanZoomBar({
        position: new OpenLayers.Pixel(2, 15)
    }));
    map.addControl(new OpenLayers.Control.Navigation());
    map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.addControl(new OpenLayers.Control.MousePosition());

    var center = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:3857"));
    map.setCenter(center, zoom);
}


function CenterMap(lon, lat) {
    
     console.log("Long: " + lon + " Lat: " + lat);
     var center = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:3857"));
     map.panTo(center);
     //map.setCenter(center, 18);
     //map.getView().setCenter(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
     
     
    /*
    var c = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:3857"));
    var duration = 2000;
    var start = +new Date();
    var pan = OpenLayers.animation.pan({
        duration: duration,
        source: (view.getCenter()),
        start: start
    });
    var bounce = OpenLayers.animation.bounce({
        duration: duration,
        resolution: 4 * view.getResolution(),
        start: start
    });
    map.beforeRender(pan, bounce);
    view.setCenter(c);
  */
}

function init() {

// Handler for .ready() called.

    // if this is just a coverage or a group of them, disable a few items,
    // and default to jpeg format
    format = 'image/png';

    if (pureCoverage) {
        document.getElementById('filterType').disabled = true;
        document.getElementById('filter').disabled = true;
        document.getElementById('antialiasSelector').disabled = true;
        document.getElementById('updateFilterButton').disabled = true;
        document.getElementById('resetFilterButton').disabled = true;
        document.getElementById('jpeg').selected = true;
    }

    var bounds = new OpenLayers.Bounds(
            -74.8073714589999, 10.3455432,
            -74.224567, 11.0085251990001
            );
    var options = {
        controls: [],
        maxExtent: bounds,
        maxResolution: 0.0025897734335941,
        projection: "EPSG:4326",
        units: 'degrees',
        sphericalMercator: true
    };
    /*
     map = new OpenLayers.Map("map", 
     {
     controls:    [new OpenLayers.Control.Navigation(), 
     new OpenLayers.Control.PanZoomBar(), 
     new OpenLayers.Control.LayerSwitcher(), 
     new OpenLayers.Control.MousePosition()], 
     numZoomLevels: 22, 
     minResolution: 19.109257068634033, 
     projection: new OpenLayers.Projection("EPSG:4326"),
     displayProjection: new OpenLayers.Projection("EPSG:4326"), 
     sphericalMercator: true
     }
     );
     */

    map = new OpenLayers.Map('map', options);

    var alcaldiaBaqLayers = new OpenLayers.Layer.WMS(
            "Alcaldia - BAQ", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '0,1,2,3,4',
                "STYLES": '',
                format: format
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: true,
        yx: {'EPSG:4326': true}
    }
    );

    var alcaldiaBaqLayersTheme = new OpenLayers.Layer.WMS(
            "Temáticas", "http://200.89.106.115/arcgis/services/Bomberos/MapServer/WMSServer",
            {
                "LAYERS": '5,6,7,8,9,10,11,12',
                "STYLES": '',
                transparent: "true",
                format: format
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );


    var googleLayers = new OpenLayers.Layer.Google(
            "Google",
            {type: google.maps.MapTypeId.ROADMAP},
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: true,
        yx: {'EPSG:4326': true}
    }
    );


    // setup tiled layer
    stationsLayer = new OpenLayers.Layer.WMS(
            "ESTACIONES", "http://webavl.bomberos.tecnosystems.co:80/geoserver/firefighters_dev_ws/wms",
            {
                "LAYERS": 'firefighters_dev_ws:estaciones',
                transparent: "true",
                "STYLES": '',
                format: format
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );

    // setup tiled layer
    machinesLayer = new OpenLayers.Layer.WMS(
            "MAQUINAS", "http://webavl.bomberos.tecnosystems.co:80/geoserver/firefighters_dev_ws/wms",
            {
                "LAYERS": 'firefighters_dev_ws:maquinas',
                transparent: "true",
                "STYLES": '',
                format: format
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );

    // setup tiled layer
    casesLayer = new OpenLayers.Layer.WMS(
            "CASOS", "http://webavl.bomberos.tecnosystems.co:80/geoserver/firefighters_dev_ws/wms",
            {
                "LAYERS": 'firefighters_dev_ws:casos',
                transparent: "true",
                "STYLES": '',
                format: format
            },
    {
        buffer: 0,
        displayOutsideMaxExtent: true,
        isBaseLayer: false,
        yx: {'EPSG:4326': true}
    }
    );

    map.addLayers([alcaldiaBaqLayers, alcaldiaBaqLayersTheme, machinesLayer, stationsLayer, casesLayer]);

    // build up all controls
    map.addControl(new OpenLayers.Control.PanZoomBar({
        position: new OpenLayers.Pixel(2, 15)
    }));
    map.addControl(new OpenLayers.Control.Navigation());
    map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.addControl(new OpenLayers.Control.MousePosition());

    map.zoomToExtent(alcaldiaBaqLayers.maxExtent);

    /*
     var center = new OpenLayers.LonLat(-74.82050, 11.00640).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
     map.setCenter(center, 14); 
     */
}

// sets the HTML provided into the nodelist element
function setHTML(response) {
    document.getElementById('nodelist').innerHTML = response.responseText;
}
;

// shows/hide the control panel
function toggleControlPanel(event) {
    var toolbar = document.getElementById("toolbar");
    if (toolbar.style.display == "none") {
        toolbar.style.display = "block";
    }
    else {
        toolbar.style.display = "none";
    }
    event.stopPropagation();
    map.updateSize()
}

// Tiling mode, can be 'tiled' or 'untiled'
function setTileMode(tilingMode) {
    if (tilingMode == 'tiled') {
        untiled.setVisibility(false);
        tiled.setVisibility(true);
        map.setBaseLayer(tiled);
    }
    else {
        untiled.setVisibility(true);
        tiled.setVisibility(false);
        map.setBaseLayer(untiled);
    }
}

// Transition effect, can be null or 'resize'
function setTransitionMode(transitionEffect) {
    if (transitionEffect === 'resize') {
        tiled.transitionEffect = transitionEffect;
        untiled.transitionEffect = transitionEffect;
    }
    else {
        tiled.transitionEffect = null;
        untiled.transitionEffect = null;
    }
}

// changes the current tile format
function setImageFormat(mime) {
    // we may be switching format on setup
    if (tiled == null)
        return;

    tiled.mergeNewParams({
        format: mime
    });
    untiled.mergeNewParams({
        format: mime
    });
    /*
     var paletteSelector = document.getElementById('paletteSelector')
     if (mime == 'image/jpeg') {
     paletteSelector.selectedIndex = 0;
     setPalette('');
     paletteSelector.disabled = true;
     }
     else {
     paletteSelector.disabled = false;
     }
     */
}

// sets the chosen style
function setStyle(style) {
    // we may be switching style on setup
    if (tiled == null)
        return;

    tiled.mergeNewParams({
        styles: style
    });
    untiled.mergeNewParams({
        styles: style
    });
}

// sets the chosen WMS version
function setWMSVersion(wmsVersion) {
    // we may be switching style on setup
    if (wmsVersion == null)
        return;

    if (wmsVersion == "1.3.0") {
        origin = map.maxExtent.bottom + ',' + map.maxExtent.left;
    } else {
        origin = map.maxExtent.left + ',' + map.maxExtent.bottom;
    }

    tiled.mergeNewParams({
        version: wmsVersion,
        tilesOrigin: origin
    });
    untiled.mergeNewParams({
        version: wmsVersion
    });
}

function setAntialiasMode(mode) {
    tiled.mergeNewParams({
        format_options: 'antialias:' + mode
    });
    untiled.mergeNewParams({
        format_options: 'antialias:' + mode
    });
}

function setPalette(mode) {
    if (mode == '') {
        tiled.mergeNewParams({
            palette: null
        });
        untiled.mergeNewParams({
            palette: null
        });
    }
    else {
        tiled.mergeNewParams({
            palette: mode
        });
        untiled.mergeNewParams({
            palette: mode
        });
    }
}

function setWidth(size) {
    var mapDiv = document.getElementById('map');
    var wrapper = document.getElementById('wrapper');

    if (size == "auto") {
        // reset back to the default value
        mapDiv.style.width = null;
        wrapper.style.width = null;
    }
    else {
        mapDiv.style.width = size + "px";
        wrapper.style.width = size + "px";
    }
    // notify OL that we changed the size of the map div
    map.updateSize();
}

function setHeight(size) {
    var mapDiv = document.getElementById('map');

    if (size == "auto") {
        // reset back to the default value
        mapDiv.style.height = null;
    }
    else {
        mapDiv.style.height = size + "px";
    }
    // notify OL that we changed the size of the map div
    map.updateSize();
}

function updateFilter() {
    if (pureCoverage)
        return;

    var filterType = document.getElementById('filterType').value;
    var filter = document.getElementById('filter').value;

    // by default, reset all filters
    var filterParams = {
        filter: null,
        cql_filter: null,
        featureId: null
    };
    if (OpenLayers.String.trim(filter) != "") {
        if (filterType == "cql")
            filterParams["cql_filter"] = filter;
        if (filterType == "ogc")
            filterParams["filter"] = filter;
        if (filterType == "fid")
            filterParams["featureId"] = filter;
    }
    // merge the new filter definitions
    mergeNewParams(filterParams);
}

function resetFilter() {
    if (pureCoverage)
        return;

    document.getElementById('filter').value = "";
    updateFilter();
}

function mergeNewParams(params) {
    tiled.mergeNewParams(params);
    untiled.mergeNewParams(params);
}

